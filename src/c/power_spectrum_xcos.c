#include <stdio.h>
#include <MALLOC.h>
#include "scicos_block4.h"
#include "localization.h"
#include "FFT.h"

void power_spectrum_xcos(scicos_block *block,int flag)
{
  /*  ipar[0] = buffer_size */
  /*  rpar[0] = padding value */
  int     i             = 0;
  int     nsamples      = block->ipar[0];
  double  padding_value = block->rpar[0];
  float * out_spectrum  = NULL;

  switch(flag)
    {
    case Initialization:
      /* the workspace is used to store previous values */
      if ((GetWorkPtrs(block) = (float *)MALLOC(sizeof(float)*nsamples))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "power_spectrum_xcos");
	  return;
	}
      
      for(i=0;i<nsamples;i++) 
      {
	((float *)GetWorkPtrs(block))[i] = (float)padding_value;
      }
      break;
    case Ending:
      /* final call to scicos - free memory */
      FREE(GetWorkPtrs(block));
      break;
    case DerivativeState:
    case StateUpdate:
      /* during state update, we fill the buffer and shift the values */

      for(i=1;i<nsamples;i++) ((float *)GetWorkPtrs(block))[i-1] = ((float *)GetWorkPtrs(block))[i];

      ((float *)GetWorkPtrs(block))[nsamples-1] = (float)((double *)GetInPortPtrs(block,1))[0];
      break;
    case OutputUpdate:
      /* during output update, we transform the buffer into an output vector */
      if ((out_spectrum = (float *)MALLOC(sizeof(float)*nsamples/2))== NULL)
	{
	  Coserror("%s: Memory allocation problem\n", "power_spectrum_xcos");
	  return;
	}

      PowerSpectrum(nsamples, (float *)GetWorkPtrs(block), out_spectrum);

      for(i=0;i<nsamples/2;i++) ((double *)GetOutPortPtrs(block,1))[i] = (double)out_spectrum[i];
      
      if (out_spectrum) FREE(out_spectrum);
      break;
    }
}
