// ====================================================================
// Yann COLLETTE
// DIGITEO 2010
// This file is released into the public domain
// ====================================================================

src_c_path = get_absolute_file_path('builder_c.sce');

// Symbols to be exported in the library
Symbols = ["PowerSpectrum","RealFFT","WindowFunc","FFT", ... 
           "buffer_vect_xcos","real_fft_xcos","power_spectrum_xcos", ...
           "window_func_xcos","inverse_fft_xcos","vector_scope_xcos", ...
           "fft_scope_xcos","ps_scope_xcos"];

// Files to compile
Files = ["buffer_vect_xcos.c","FFT.c","power_spectrum_xcos.c", ...
         "real_fft_xcos.c","window_func_xcos.c","inverse_fft_xcos.c", ...
         "vector_scope_xcos.c","fft_scope_xcos.c","ps_scope_xcos.c"];

// Extra libraries
Libs = [SCI+'/bin/scicos';SCI+'/bin/scicos_blocks';SCI+'/bin/string';SCI+'/bin/scirenderer';SCI+'/bin/graphics'];

cflags  = '-I' + src_c_path;
cflags  = cflags + ' -I' + SCI + '/modules/scicos/includes';
cflags  = cflags + ' -I' + SCI + '/modules/scicos_blocks/includes';
cflags  = cflags + ' -I' + SCI + '/modules/graphics/includes';
cflags  = cflags + ' -I' + SCI + '/libs/doublylinkedlist/includes/';
cflags  = cflags + ' -I' + SCI + '/modules/renderer/includes';
cflags  = cflags + ' -I' + SCI + '/modules/string/includes';
ldflags = '' ;

tbx_build_src(Symbols, Files, 'c', src_c_path, Libs, ldflags, cflags);

clear tbx_build_src;
clear src_c_path;
clear cflags;
clear ldflags;
clear Libs;
clear Files;
clear Symbols;
