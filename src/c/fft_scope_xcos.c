/*
 * Copyright (C) 2010 - DIGITEO - Yann COLLETTE
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "CurrentObjectsManagement.h"
#include "scicos.h"
#include "scoMemoryScope.h"
#include "scoWindowScope.h"
#include "scoMisc.h"
#include "scoGetProperty.h"
#include "scoSetProperty.h"
#include "scicos_block4.h"
#include "SetJavaProperty.h"
#include "MALLOC.h"
#include "dynlib_scicos_blocks.h"
#include "FFT.h"
#include "cvstr.h"

#define MAX(A,B) ((A<B)?B:A)
#define MIN(A,B) ((A<B)?A:B)

/*
  fft_scope_draw(scicos_block * block, ScopeMemory ** pScopeMemory, int firstdraw)
  Function to draw or redraw the window
*/

/* IPAR content
   ipar[0] = nb curves + labels
   ipar[1] to ipar[ipar[0]+1] = length of each labels
   ipar[ipar[0]+1+1] to ipar[ipar[0]+1+1+ipar[1]] = first label
   ipar[ipar[0]+1+1+1+ipar[1]] to ipar[ipar[0]+1+1+1+ipar[1]+ipar[2]] = second label
   ...
   ipar[??] = color of first curve
   ipar[??+1] = color of second curve
   ...
   ipar[??] = win_id
   ipar[??] = buffer_size
   ipar[??] = [win_pos win_pos]
   ipar[??] = [win_dim win_dim]
   ipar[??] = window_type
   ipar[??] = adaptive_amp
   ipar[??] = legend
   ipar[??] = semilogx
   ipar[??] = grid */

int parse_ipar_fft_scope(scicos_block * block, int * nb_curves, char *** Labels, int ** colors, int * win_id, 
                         int * buffer_size, int ** win_pos, int ** win_dim, int * window_type, int * adaptive_amp, int * legend,
                         int * semilogx, int * grid)
{
  /* We allocate Labels and colors
     The user needs to ship all the other parameters */
  int * ipar, i, j, cv_len, offset = 0;
  int Index = 0;

  ipar  = GetIparPtrs(block);

  /* Extraction of the Labels */
  (*nb_curves) = ipar[0];
  (*Labels) = (char **)MALLOC((*nb_curves)*sizeof(char *));
  if ((*Labels) == NULL) return -1;
  (*colors) = (int *)MALLOC((*nb_curves)*sizeof(int));
  if ((*colors) == NULL) 
    {
      FREE((*Labels));
      return -1;
    }
  offset += 1 + (*nb_curves);
  for(i=0; i<(*nb_curves); i++)
    {
      /* convert scilab code of the variable name to C string */
      cv_len = ipar[i+1];
      (*Labels)[i] = (char *)MALLOC((cv_len+1)*sizeof(char));
      if ((*Labels)[i] == NULL) 
        {
          FREE((*Labels));
          FREE((*colors));
          freeArrayOfString((*Labels), i);
          return -1;
        }
      C2F(cvstr)(&cv_len,&(ipar[offset]),(*Labels)[i],(j=1,&j),(unsigned long)cv_len);
      (*Labels)[i][cv_len] = '\0';
      offset += cv_len;
    }

  /* Extraction of the colors */
  for(i=0; i<(*nb_curves); i++)
    {
      (*colors)[i] = ipar[offset+i];
    }

  offset += (*nb_curves);

  /* extraction of the other parameters */
  (*win_id)       = ipar[offset+0];
  (*buffer_size)  = ipar[offset+1];
  (*win_pos)[0]   = ipar[offset+2];
  (*win_pos)[1]   = ipar[offset+3];
  (*win_dim)[0]   = ipar[offset+4];
  (*win_dim)[1]   = ipar[offset+5];
  (*window_type)  = ipar[offset+6];
  (*adaptive_amp) = ipar[offset+7];
  (*legend)       = ipar[offset+8];
  (*semilogx)     = ipar[offset+9];
  (*grid)         = ipar[offset+10];

  return 0;
}

/* RPAR content
   rpar[0] = amp_min
   rpar[1] = amp_max
   rpar[2] = refresh_period
   rpar[3] = fmin
   rpar[4] = fmax
   rpar[5] = padding value
   rpar[6] = sampling freq
   rpar[7] = alpha (a parameter for some windows weighting func) */

int parse_rpar_fft_scope(scicos_block * block, double * amp_min, double * amp_max, double * refresh_period, 
                         double * fmin, double * fmax, double * padding_value, double * sampling_freq,
                         double * alpha)
{
  double * rpar = GetRparPtrs(block);

  (*amp_min)        = rpar[0];
  (*amp_max)        = rpar[1];
  (*refresh_period) = rpar[2];
  (*fmin)           = rpar[3];
  (*fmax)           = rpar[4];
  (*padding_value)  = rpar[5];
  (*sampling_freq)  = rpar[6];
  (*alpha)          = rpar[7];

  return 0;
}

/* buffer_size must be a power of 2 !! */

/* This structure will handle all the intermediate memory needed
   for:
   - the scope (the scope data are stored in pScopeMemory
   - the input signal (it needs to be converted to float)
   - the fft results (which needs to be stored in float format) */

struct fft_scope_memory
{
  ScopeMemory * pScopeMemory;
  float ** input_signal;
  float  * weighted_input_signal;
  float  * real_fft;
  float  * imag_fft;
  double   amp_min;
  double   amp_max;
  double   fmin;
  double   fmax;
  double   refresh_period;
  double   padding_value;
  double   sampling_freq;
  double   alpha;
  int      buffer_size;
  int    * win_pos;
  int    * win_dim;
  int      win;
  int      window_type;
  int      adaptive_amp;
  int      legend;
  int      semilogx;
  int      grid;
  int      nb_curves;
  int    * colors;
  char  ** Legend_Labels;
  long long * tabofhandles;
};

void fft_scope_draw(scicos_block * block, struct fft_scope_memory ** pFFTScopeMemory, int firstdraw)
{
  int    i = 0, ierror = 0;
  int    dimension = 0;
  int    line_size = 2;
  int    number_of_subwin = 0;
  int    number_of_curves_by_subwin[1];
  char * label = NULL;
  void * block_work_fake = NULL;

  if (firstdraw == 1)
    {
      (*pFFTScopeMemory) = (struct fft_scope_memory *)MALLOC(1*sizeof(struct fft_scope_memory));
      
      (*pFFTScopeMemory)->win_pos = (int *)MALLOC(2*sizeof(int));
      (*pFFTScopeMemory)->win_dim = (int *)MALLOC(2*sizeof(int));

      /* Retrieving IPAR and RPAR Parameters */
      
      ierror = parse_ipar_fft_scope(block, &((*pFFTScopeMemory)->nb_curves), &((*pFFTScopeMemory)->Legend_Labels), &((*pFFTScopeMemory)->colors),
                                    &((*pFFTScopeMemory)->win), &((*pFFTScopeMemory)->buffer_size), &((*pFFTScopeMemory)->win_pos), 
                                    &((*pFFTScopeMemory)->win_dim), &((*pFFTScopeMemory)->window_type), &((*pFFTScopeMemory)->adaptive_amp), 
                                    &((*pFFTScopeMemory)->legend), &((*pFFTScopeMemory)->semilogx), &((*pFFTScopeMemory)->grid));

      if (ierror)
        {
          Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
          return;
        }

      ierror = parse_rpar_fft_scope(block, &((*pFFTScopeMemory)->amp_min), &((*pFFTScopeMemory)->amp_max), &((*pFFTScopeMemory)->refresh_period), 
                                    &((*pFFTScopeMemory)->fmin), &((*pFFTScopeMemory)->fmax), &((*pFFTScopeMemory)->padding_value), 
                                    &((*pFFTScopeMemory)->sampling_freq), &((*pFFTScopeMemory)->alpha));
      
      if (ierror)
        {
          Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
          return;
        }
    }

  /* Here, we store the maximum number of curves by subwin we can plot.
     This is the size of the number of colors stored in the colors list.
     If we have more curves, they will be printed using black color */

  number_of_curves_by_subwin[0] = (*pFFTScopeMemory)->nb_curves;
  number_of_subwin = 1;
  dimension        = 2; // 2D graph

  if ((*pFFTScopeMemory)->legend)
    {
      if (GetInPortRows(block,1)>(*pFFTScopeMemory)->nb_curves)
        {
          Coserror("%s: Not enough labels\n", "fft_scope_xcos");
          FREE((*pFFTScopeMemory)->win_pos);
          FREE((*pFFTScopeMemory)->win_dim);
          FREE((*pFFTScopeMemory)->colors);
          freeArrayOfString((*pFFTScopeMemory)->Legend_Labels, (*pFFTScopeMemory)->nb_curves);
          return;
        }
    }

  (*pFFTScopeMemory)->tabofhandles = (long long *)MALLOC(GetInPortRows(block,1)*sizeof(long long));

  /* A minimum frequency is required if we want to plot a semilogx graph */
  (*pFFTScopeMemory)->fmin = 2*M_PI*(*pFFTScopeMemory)->sampling_freq/2*(1)/(*pFFTScopeMemory)->buffer_size;
  (*pFFTScopeMemory)->fmax = 2*M_PI*(*pFFTScopeMemory)->sampling_freq/2*((*pFFTScopeMemory)->buffer_size-1)/(*pFFTScopeMemory)->buffer_size;
  label = GetLabelPtrs(block);

  /* Allocating memory */
  if (firstdraw == 1)
    {
      (*pFFTScopeMemory)->input_signal = (float **)MALLOC(GetInPortRows(block,1)*sizeof(float *));
      for(i=0;i<GetInPortRows(block,1); i++)
        {
          (*pFFTScopeMemory)->input_signal[i] = (float *)MALLOC((*pFFTScopeMemory)->buffer_size*sizeof(float));
          if ((*pFFTScopeMemory)->input_signal[i]==NULL)
            {
              Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
              FREE((*pFFTScopeMemory)->win_pos);
              FREE((*pFFTScopeMemory)->win_dim);
              FREE((*pFFTScopeMemory)->colors);
              freeArrayOfString((*pFFTScopeMemory)->Legend_Labels, (*pFFTScopeMemory)->nb_curves);
              FREE((*pFFTScopeMemory)->input_signal);
              FREE((*pFFTScopeMemory)->tabofhandles);
              freeArray((*pFFTScopeMemory)->input_signal, i);
              return;
            }
        }

      (*pFFTScopeMemory)->weighted_input_signal = (float *)MALLOC((*pFFTScopeMemory)->buffer_size*sizeof(float));
      if ((*pFFTScopeMemory)->weighted_input_signal==NULL)
        {
	  Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
          FREE((*pFFTScopeMemory)->win_pos);
          FREE((*pFFTScopeMemory)->win_dim);
          FREE((*pFFTScopeMemory)->colors);
          freeArrayOfString((*pFFTScopeMemory)->Legend_Labels, (*pFFTScopeMemory)->nb_curves);
          freeArray((*pFFTScopeMemory)->input_signal, GetInPortRows(block,1));
          FREE((*pFFTScopeMemory)->tabofhandles);
	  return;
        }

      (*pFFTScopeMemory)->real_fft = (float *)MALLOC((*pFFTScopeMemory)->buffer_size/2*sizeof(float));
      if ((*pFFTScopeMemory)->real_fft==NULL)
        {
	  Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
          FREE((*pFFTScopeMemory)->win_pos);
          FREE((*pFFTScopeMemory)->win_dim);
          FREE((*pFFTScopeMemory)->colors);
          freeArrayOfString((*pFFTScopeMemory)->Legend_Labels, (*pFFTScopeMemory)->nb_curves);
          freeArray((*pFFTScopeMemory)->input_signal, GetInPortRows(block,1));
          FREE((*pFFTScopeMemory)->weighted_input_signal);
          FREE((*pFFTScopeMemory)->tabofhandles);
	  return;
        }

      (*pFFTScopeMemory)->imag_fft = (float *)MALLOC((*pFFTScopeMemory)->buffer_size/2*sizeof(float));
      if ((*pFFTScopeMemory)->imag_fft==NULL)
        {
	  Coserror("%s: Memory allocation problem\n", "fft_scope_xcos");
          FREE((*pFFTScopeMemory)->win_pos);
          FREE((*pFFTScopeMemory)->win_dim);
          FREE((*pFFTScopeMemory)->colors);
          freeArrayOfString((*pFFTScopeMemory)->Legend_Labels, (*pFFTScopeMemory)->nb_curves);
          freeArray((*pFFTScopeMemory)->input_signal, GetInPortRows(block,1));
          FREE((*pFFTScopeMemory)->weighted_input_signal);
          FREE((*pFFTScopeMemory)->real_fft);
          FREE((*pFFTScopeMemory)->tabofhandles);
	  return;
        }

      /* block_work_fake is here to "fill" a missing parameter which is not used anymore
         because we store the memory in a special structure */

      scoInitScopeMemory(&block_work_fake,&((*pFFTScopeMemory)->pScopeMemory), number_of_subwin, number_of_curves_by_subwin);

      /* Allocate size for LongDraw and ShortDraw */
      scoSetLongDrawSize((*pFFTScopeMemory)->pScopeMemory, 0,(*pFFTScopeMemory)->buffer_size/2);
      scoSetShortDrawSize((*pFFTScopeMemory)->pScopeMemory,0,(*pFFTScopeMemory)->buffer_size/2);
      scoSetPeriod((*pFFTScopeMemory)->pScopeMemory,0,(*pFFTScopeMemory)->fmax+(*pFFTScopeMemory)->fmin);
    }

  /* Creation of the Scope */
  scoInitOfWindow((*pFFTScopeMemory)->pScopeMemory, dimension, (*pFFTScopeMemory)->win, (*pFFTScopeMemory)->win_pos, 
                  (*pFFTScopeMemory)->win_dim, &((*pFFTScopeMemory)->fmin), &((*pFFTScopeMemory)->fmax), 
                  &((*pFFTScopeMemory)->amp_min), &((*pFFTScopeMemory)->amp_max), NULL, NULL);

  if (scoGetScopeActivation((*pFFTScopeMemory)->pScopeMemory) == 1)
    {
      scoAddTitlesScope((*pFFTScopeMemory)->pScopeMemory, label, "freq.", "amp.",NULL);
      scoAddCoupleOfPolylines((*pFFTScopeMemory)->pScopeMemory,(*pFFTScopeMemory)->colors);
    }

  if (scoGetPointerScopeWindow((*pFFTScopeMemory)->pScopeMemory) != NULL)
    {
      sciSetJavaUseSingleBuffer(scoGetPointerScopeWindow((*pFFTScopeMemory)->pScopeMemory), TRUE);
    }
 
  *block->work = *pFFTScopeMemory;
}

/*
  void fft_scope(scicos_block * block,int flag)
  the computational function
  block A pointer to a scicos_block
  flag An int which indicates the state of the block (init, update, ending)
*/

void fft_scope_xcos(scicos_block * block,int flag)
{
  struct fft_scope_memory * pFFTScopeMemory = NULL;
  int      i = 0, j = 0;
  double   t = 0, amp_tmp, amp_min, amp_max;
  scoGraphicalObject pLongDraw;
  scoGraphicalObject pAxes;
  int      line_size = 2;
  void  * block_work_fake = NULL;
  int * test_tmp = NULL;
  sciPointObj * Leg = NULL;

  switch(flag) 
    {
    case Initialization:
      {
	fft_scope_draw(block, &pFFTScopeMemory, 1);

        /* Init the time section */
        pFFTScopeMemory->pScopeMemory->d_last_scope_update_time = get_scicos_time();

        for(i=0; i<GetInPortRows(block,1); i++)
          {
            for(j=0;j<pFFTScopeMemory->buffer_size;j++)
              {
                pFFTScopeMemory->input_signal[i][j] = pFFTScopeMemory->padding_value;
              }
          }

	break;
      }
    
    case OutputUpdate:
      {
        pFFTScopeMemory = (struct fft_scope_memory *)*block->work;

        for(i=0; i<GetInPortRows(block,1); i++)
          {
            /* during state update, we fill the buffer and shift the values */
            for(j=1;j<pFFTScopeMemory->buffer_size;j++) pFFTScopeMemory->input_signal[i][j-1] = pFFTScopeMemory->input_signal[i][j];
            pFFTScopeMemory->input_signal[i][pFFTScopeMemory->buffer_size-1] = (float)((double *)GetRealInPortPtrs(block,1))[i];
          }

        if (scoGetScopeActivation(pFFTScopeMemory->pScopeMemory) == 1)
          {
	    t = get_scicos_time();

            if ((t - pFFTScopeMemory->pScopeMemory->d_last_scope_update_time > pFFTScopeMemory->refresh_period))
              {
                /* We force a redraw to clean the preceding curve */
                fft_scope_draw(block,&pFFTScopeMemory,0);
                
                for(i=0; i<GetInPortRows(block,1); i++)
                  {
                    /* Window weighing */
                    memcpy(pFFTScopeMemory->weighted_input_signal, pFFTScopeMemory->input_signal[i], pFFTScopeMemory->buffer_size*sizeof(float));
                    WindowFunc((windowfunc_t)pFFTScopeMemory->window_type, 
                               pFFTScopeMemory->buffer_size, 
                               pFFTScopeMemory->weighted_input_signal,
                               (float)pFFTScopeMemory->alpha);
                    
                    /* Now perform the FFT */
                    RealFFT(pFFTScopeMemory->buffer_size, pFFTScopeMemory->weighted_input_signal, pFFTScopeMemory->real_fft, pFFTScopeMemory->imag_fft);
                    
                    /* nb_curves to plot = GetNin(block); */
                    pLongDraw  = scoGetPointerLongDraw(pFFTScopeMemory->pScopeMemory,0,i);
                    if (pLongDraw)
                      {
                        if (pFFTScopeMemory->adaptive_amp)
                          {
                            amp_min = 10*log10(pow((double)pFFTScopeMemory->real_fft[0],2.0) + 
                                               pow((double)pFFTScopeMemory->imag_fft[0],2.0));
                            amp_max = amp_min;
                            
                            for(j=0; j<pFFTScopeMemory->buffer_size/2; j++)
                              {
                                amp_tmp = 10*log10(pow((double)pFFTScopeMemory->real_fft[j],2.0) + 
                                                   pow((double)pFFTScopeMemory->imag_fft[j],2.0));
                                
                                if (amp_tmp<amp_min) amp_min = amp_tmp;
                                if (amp_tmp>amp_max) amp_max = amp_tmp;
                              }
                            
                            pAxes = scoGetPointerAxes(pFFTScopeMemory->pScopeMemory,0);
                            pSUBWIN_FEATURE(pAxes)->SRect[2] = amp_min;
                            pSUBWIN_FEATURE(pAxes)->SRect[3] = amp_max;
                          }
                        
                        for(j=0; j<pFFTScopeMemory->buffer_size/2; j++)
                          {
                            pPOLYLINE_FEATURE(pLongDraw)->pvx[j] = (pFFTScopeMemory->fmax - pFFTScopeMemory->fmin)*j / 
                                                                   (pFFTScopeMemory->buffer_size/2-1)+pFFTScopeMemory->fmin;
                            pPOLYLINE_FEATURE(pLongDraw)->pvy[j] = 10*log10(pow((double)pFFTScopeMemory->real_fft[j],2.0) + 
                                                                            pow((double)pFFTScopeMemory->imag_fft[j],2.0));
                            pPOLYLINE_FEATURE(pLongDraw)->n1 = j+1;
                          }
                        
                        if (pFFTScopeMemory->legend)
                          {
                            pFFTScopeMemory->tabofhandles[i] = sciGetHandle(pLongDraw);
                          }
                      }
                  }

                if (pFFTScopeMemory->legend)
                  {
                    Leg = ConstructLegend(sciGetCurrentSubWin(),
                                          pFFTScopeMemory->Legend_Labels,
                                          pFFTScopeMemory->tabofhandles,
                                          GetInPortRows(block,1));
                    
                    if (Leg != NULL)
                      {
                        /* Possible positions for the Legend (defined in ObjectStructure.h)
                           SCI_LEGEND_IN_UPPER_LEFT
                           SCI_LEGEND_IN_UPPER_RIGHT
                           SCI_LEGEND_IN_LOWER_LEFT
                           SCI_LEGEND_IN_LOWER_RIGHT
                           SCI_LEGEND_BY_COORDINATES
                           SCI_LEGEND_POSITION_UNSPECIFIED
                           SCI_LEGEND_OUT_UPPER_LEFT
                           SCI_LEGEND_OUT_UPPER_RIGHT
                           SCI_LEGEND_OUT_LOWER_LEFT
                           SCI_LEGEND_OUT_LOWER_RIGHT
                           SCI_LEGEND_UPPER_CAPTION
                           SCI_LEGEND_LOWER_CAPTION */
                        
                        pLEGEND_FEATURE(Leg)->place = SCI_LEGEND_IN_UPPER_RIGHT;
                        sciSetIsFilled (Leg, FALSE);
                        sciSetIsLine (Leg, FALSE); /* Add or not a rectangle around legend block */
                        sciSetCurrentObj(Leg); 
                      }
                        
                    if (pFFTScopeMemory->semilogx)
                      {
                        pSUBWIN_FEATURE(pAxes)->logflags[0] = 'l';
                        pSUBWIN_FEATURE(pAxes)->logflags[1] = 'n';
                        pSUBWIN_FEATURE(pAxes)->logflags[2] = 'n';
                      }

                    if (pFFTScopeMemory->grid)
                      {
                        pSUBWIN_FEATURE(pAxes)->grid[0] = 1;
                        pSUBWIN_FEATURE(pAxes)->grid[1] = 1;
                        pSUBWIN_FEATURE(pAxes)->grid[2] = -1;
                      }

                    forceRedraw(pAxes);
                  }
                
                pFFTScopeMemory->pScopeMemory->d_last_scope_update_time = get_scicos_time();
                scoDrawScopeAmplitudeTimeStyle(pFFTScopeMemory->pScopeMemory, pFFTScopeMemory->pScopeMemory->d_last_scope_update_time);
              }
          }

	break;
      }
    case Ending:
      {
        pFFTScopeMemory = (struct fft_scope_memory *)*block->work;

        freeArray(pFFTScopeMemory->input_signal, GetInPortRows(block,1));
        FREE(pFFTScopeMemory->weighted_input_signal);
        FREE(pFFTScopeMemory->real_fft);
        FREE(pFFTScopeMemory->imag_fft);
        freeArrayOfString(pFFTScopeMemory->Legend_Labels, pFFTScopeMemory->nb_curves);
        FREE(pFFTScopeMemory->colors);
        FREE(pFFTScopeMemory->tabofhandles);

	if (scoGetScopeActivation(pFFTScopeMemory->pScopeMemory) == 1)
	  {
            /* Check if figure is still opened, otherwise, don't try to destroy it again. */
            scoGraphicalObject figure = scoGetPointerScopeWindow(pFFTScopeMemory->pScopeMemory);
            if (figure != NULL)
              {
                clearUserData(figure);
                /* restore double buffering */
                sciSetJavaUseSingleBuffer(figure, FALSE);
                scoDelCoupleOfPolylines(pFFTScopeMemory->pScopeMemory);
              }
	  }
        
        /* block_work_safe is here to add a missing parameter in scoFreeScopeMemory
           We store another kind of structure in block->work: a struct fft_scope_memory */
        block_work_fake = (int *)MALLOC(1*sizeof(int));
	scoFreeScopeMemory(&block_work_fake, &(pFFTScopeMemory->pScopeMemory));
        break;  
      }
    }
}
